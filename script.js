function date_now() {
  const weekdays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  const now = new Date();

  let date_day = now.getDate() < 10 ? "0" + now.getDate() : now.getDate();
  let date_month = now.getMonth() < 10 ? "0" + now.getMonth() : now.getMonth();
  let time_hours = now.getHours() < 10 ? "0" + now.getHours() : now.getHours();
  let time_minutes =
    now.getMinutes() < 10 ? "0" + now.getMinutes() : now.getMinutes();
  let time_seconds =
    now.getSeconds() < 10 ? "0" + now.getSeconds() : now.getSeconds();

  let date = `${date_day}/${date_month}/${now.getFullYear()}`;
  let time = `${time_hours}:${time_minutes}:${time_seconds}`;

  document.getElementById("clock_weekday").innerHTML = weekdays[now.getDay()];
  document.getElementById("clock_date").innerHTML = date;
  document.getElementById("clock_time").innerHTML = time;
}

setInterval(date_now, 1000);
